// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class cool_arcanoidEditorTarget : TargetRules
{
	public cool_arcanoidEditorTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "cool_arcanoid" } );
		
		//bCompileChaos = true;
		//bUseChaos = true;
	}
}
