// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CA_UserWidget.generated.h"

/**
 * 
 */
UCLASS()
class COOL_ARCANOID_API UCA_UserWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateLifes(int lifes);
	UFUNCTION(BlueprintImplementableEvent)
	void UpdateScore(int score);
	UFUNCTION(BlueprintImplementableEvent)
	void UpdatePrevScore(int score);
};
