// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/DataTable.h"
#include "../Game/CA_GameInstance.h"
#include "CA_Bonus.generated.h"

class ACA_Bat;
class ACA_Bonus;

UENUM(BlueprintType)
enum class EBonusClass : uint8 {
	Default			UMETA(DisplayName = "Default"),
	Expand			UMETA(DisplayName = "Expand"),
	Narrow			UMETA(DisplayName = "Narrow"),
	ExtraBall		UMETA(DisplayName = "ExtraBall"),
	Explosion		UMETA(DisplayName = "Explosion"),
	BallFast		UMETA(DisplayName = "BallFast"),
	BallSlow		UMETA(DisplayName = "BallSlow"),
	BallPenetration	UMETA(DisplayName = "BallPenetration"),
};

USTRUCT(BlueprintType)
struct FBonusSpawnProperty : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:

	FBonusSpawnProperty()
	{}

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ACA_Bonus> BonusClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BonusSpawnProb;
};

UCLASS()
class COOL_ARCANOID_API ACA_Bonus : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACA_Bonus();

protected:
	UFUNCTION(BlueprintNativeEvent)
	void BeginPlay() override;
	void BeginPlay_Implementation();

	UFUNCTION(BlueprintCallable)
	void BonusFall(float DeltaTime);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float FallSpeed = 600;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FRotator SpinSpeed = FRotator();

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	UStaticMeshComponent* BonusMesh;

	UFUNCTION()
	void BeginOverlap(AActor* Actor, AActor* OtherActor);
	UFUNCTION()
	void EndOverlap(UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex);

	UFUNCTION()
	void EndGame(EGameEnding Ending);

public:
	UFUNCTION(BlueprintNativeEvent)
	void Tick(float DeltaTime) override;
	void Tick_Implementation(float DeltaTime);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void ApplyEffect(ACA_Bat* Player);
	void ApplyEffect_Implementation(ACA_Bat* Player);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	EBonusClass GetBonusClass() const;
	EBonusClass GetBonusClass_Implementation() const;

};
