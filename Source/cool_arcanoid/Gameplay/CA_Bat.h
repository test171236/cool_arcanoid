// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "CA_Bat.generated.h"

class ACA_Ball;
class ACA_Bonus;
class UCA_BonusManager;

UCLASS()
class COOL_ARCANOID_API ACA_Bat : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACA_Bat();

	UFUNCTION()
	void SpawnBall();

	UFUNCTION()
	void OnMove(float Axis);

	UFUNCTION()
	void OnAction();
	
	UFUNCTION()
	void OnExit();

	UFUNCTION()
	void DestroyBall(ACA_Ball *b);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float MoveSpeed = 1000;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	UCA_BonusManager* BonusManager;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	UStaticMeshComponent* BatMesh;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TSubclassOf<ACA_Ball> BallClass;

	UFUNCTION()
	void EndGame(EGameEnding Ending);

public:
	UPROPERTY()
	TArray<ACA_Ball*> AllBalls;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	FVector BaseScale = FVector(5, 1, 1);
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	float BaseLen;

	UFUNCTION(BlueprintCallable)
	void AddBonus(ACA_Bonus* Bonus);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
