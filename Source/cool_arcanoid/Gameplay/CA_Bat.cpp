// Fill out your copyright notice in the Description page of Project Settings.

#include "CA_Bat.h"

#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "../Map/CA_Wall.h"
#include "../Map/CA_MapBase.h"
#include "Engine/Classes/Components/CapsuleComponent.h"
#include "CA_Ball.h"
#include "CA_Bonus.h"
#include "CA_BonusManager.h"
#include "../Game/CA_GameInstance.h"
#include "../Game/CA_GameStateBase.h"

// Sets default values
ACA_Bat::ACA_Bat()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	BatMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BatMesh"));
	SetRootComponent(BatMesh);
	BatMesh->AddLocalOffset(FVector(0, 0, 50));
	BatMesh->SetCollisionProfileName(TEXT("Pawn"));
	SetActorEnableCollision(true);

	BonusManager = CreateDefaultSubobject<UCA_BonusManager>(TEXT("BonusManager"));
	BonusManager->SetAutoActivate(true);
}

void ACA_Bat::OnMove(float Axis)
{

	float delta_x = GetWorld()->DeltaTimeSeconds * MoveSpeed * Axis;

	FHitResult hr;

	AddActorWorldOffset(FVector(delta_x, 0, 0), true, &hr);

	if (hr.bStartPenetrating)
	{
		float dir = 0.f;
		UCA_Wall *wall = Cast<UCA_Wall>(hr.Component);
		ACA_MapBase *map = Cast<ACA_MapBase>(hr.Actor);

		if (IsValid(wall) && wall == map->LeftWall)
			dir = 1.f;
		else if (IsValid(wall) && wall == map->RightWall)
			dir = -1.f;

		AddActorWorldOffset(FVector(10.f * dir, 0, 0), false);
	} 
}

void ACA_Bat::SpawnBall(void)
{
	FActorSpawnParameters asp;
	asp.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
	
	float locX = GetActorLocation().X;
	float locZ = GetActorLocation().Z;
	ACA_Ball* b;

	int i = 0;
	do {
		if (i%2)
			b = GetWorld()->SpawnActorAbsolute<ACA_Ball>(BallClass,
				FTransform(FVector(locX + i/2 * 10.f, 0, locZ + 110)),
				asp);
		else
			b = GetWorld()->SpawnActorAbsolute<ACA_Ball>(BallClass,
				FTransform(FVector(locX - i/2 * 10.f, 0, locZ + 110)),
				asp);

		i++;
	} while (!IsValid(b));
	
	if (IsValid(b))
		AllBalls.Add(b);
}

void ACA_Bat::OnAction(void)
{
	ACA_GameStateBase *gsb = Cast<ACA_GameStateBase>(GetWorld()->GetGameState());
	if (IsValid(gsb) && !gsb->IsGameStarted)
	{
		UCA_GameInstance* gi = Cast<UCA_GameInstance>(GetGameInstance());
		gi->m_OnGameStarted.Broadcast();
		SpawnBall();
	}
}

void ACA_Bat::OnExit()
{
	UKismetSystemLibrary::QuitGame(GetWorld(), nullptr, EQuitPreference::Quit, false);
}

void ACA_Bat::DestroyBall(ACA_Ball* b)
{
	AllBalls.Remove(b);
	b->Destroy();

	if (AllBalls.Num() < 1)
	{
		SpawnBall();

		ACA_GameStateBase* gsb = Cast<ACA_GameStateBase>(GetWorld()->GetGameState());
		if (IsValid(gsb))
			gsb->Died();
	}
}

// Called when the game starts or when spawned
void ACA_Bat::BeginPlay()
{
	Super::BeginPlay();
	
	APlayerController* apc = UGameplayStatics::GetPlayerController(this, 0);

	apc->Possess(this);

	UCA_GameInstance* gi = Cast<UCA_GameInstance>(GetGameInstance());
	gi->m_OnGameEnd.AddDynamic(this, &ACA_Bat::EndGame);

}

void ACA_Bat::EndGame(EGameEnding Ending)
{
	BonusManager->RemoveAllBonuses();

	for (auto b : AllBalls)
		b->Destroy();
	AllBalls.Empty();
}

void ACA_Bat::AddBonus(ACA_Bonus* Bonus)
{
	BonusManager->AddBonus(Bonus);
}

// Called every frame
void ACA_Bat::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACA_Bat::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAction("Action", EInputEvent::IE_Pressed, this, &ACA_Bat::OnAction);
	InputComponent->BindAction("Exit", EInputEvent::IE_Pressed, this, &ACA_Bat::OnExit);

	InputComponent->BindAxis("Move", this, &ACA_Bat::OnMove);
}
