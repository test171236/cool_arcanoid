// Fill out your copyright notice in the Description page of Project Settings.


#include "CA_Block.h"
#include "CA_Ball.h"
#include "../Map/CA_MapBase.h"
#include "../Game/CA_GameStateBase.h"
#include "../Game/CA_GameInstance.h"
#include "CA_Bonus.h"

// Sets default values
ACA_Block::ACA_Block()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockMesh"));
	SetRootComponent(BlockMesh);
	BlockMesh->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	SetActorEnableCollision(true);

	OnActorHit.AddDynamic(this, &ACA_Block::OnHit);

	static ConstructorHelpers::FObjectFinder<UDataTable> BonusSpawnObject(
		TEXT("DataTable'/Game/Blueprints/Gameplay/Bonuses/BonusSpawn.BonusSpawn'"));
	if (BonusSpawnObject.Succeeded())
	{
		BonusSpawnTable = BonusSpawnObject.Object;
	}
}

// Called when the game starts or when spawned
void ACA_Block::BeginPlay_Implementation()
{
	Super::BeginPlay();
	
}

void ACA_Block::TakeDamage_HP()
{
	if (--HP == 0)
		Break();
}


void ACA_Block::OnHit_Implementation(AActor* Actor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	ACA_Ball* ball = Cast<ACA_Ball>(OtherActor);

	if (IsValid(ball))
	{
		if (GetWorld()->TimeSeconds - LastHitTime < 0.03)
			return;
		LastHitTime = GetWorld()->TimeSeconds;

		TakeDamage_HP();
	}
}

void ACA_Block::Break()
{
	SpawnBonus();

	ACA_MapBase* map = Cast<ACA_MapBase>(GetOwner());
	if (IsValid(map))
		map->OnBlockBreak(this);

	Destroy();
}

void ACA_Block::SpawnBonus()
{
	float border = 0.f;
	float rand = FMath::FRand();
	for (auto row : BonusSpawnTable->GetRowMap())
	{
		FBonusSpawnProperty *sp = (FBonusSpawnProperty*)row.Value;

		border += sp->BonusSpawnProb;

		if (rand <= border)
		{
			FActorSpawnParameters asp;
			asp.Owner = GetOwner();

			GetWorld()->SpawnActorAbsolute(
				sp->BonusClass,
				FTransform(GetActorLocation()),
				asp
			);
			break;
		}
	}
}

// Called every frame
void ACA_Block::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float alpha = MovingSpeed * DeltaTime / (StartPoint - EndPoint).Size();
	CurAlphaMoving += alpha;
	FVector loc;

	if (CurAlphaMoving >= 1) {
		MovingSpeed = -abs(MovingSpeed);
		loc = EndPoint;
	} else if (CurAlphaMoving <= 0) {
		MovingSpeed = abs(MovingSpeed);
		loc = StartPoint;
	} else {
		loc = (1 - CurAlphaMoving) * StartPoint + CurAlphaMoving * EndPoint;
	}
	
	FHitResult hr;
	SetActorLocation(loc, true, &hr);
	if (hr.bBlockingHit)
	{
		MovingSpeed = -MovingSpeed;
	}
}

void ACA_Block::SetSize(float newSizeX, float newSizeY)
{
	SizeX = newSizeX; 
	SizeY = newSizeY;
}

void ACA_Block::SetHP_Implementation(int newHP)
{
	HP = newHP;
}

void ACA_Block::SetMoving_Implementation(bool newIsMoving, const FVector& P1 = FVector(0), const FVector& P2 = FVector(0))
{
	IsMoving = newIsMoving;
	StartPoint = P1;
	EndPoint = P2;

	PrimaryActorTick.bCanEverTick = IsMoving;
	PrimaryActorTick.SetTickFunctionEnable(IsMoving);

}

void ACA_Block::SetTransparent_Implementation(bool newIsTr)
{
	IsTrasparent = newIsTr;
}
