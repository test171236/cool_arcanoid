// Fill out your copyright notice in the Description page of Project Settings.


#include "CA_Bonus.h"

#include "CA_Bat.h"
#include "../Map/CA_MapBase.h"
#include "../Game/CA_GameInstance.h"

// Sets default values
ACA_Bonus::ACA_Bonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	BonusMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BonusMesh"));
	SetRootComponent(BonusMesh);
	BonusMesh->SetCollisionProfileName(TEXT("OverlapAllDynamic"));

	OnActorBeginOverlap.AddDynamic(this, &ACA_Bonus::BeginOverlap);
	BonusMesh->OnComponentEndOverlap.AddDynamic(this, &ACA_Bonus::EndOverlap);
}

void ACA_Bonus::BeginOverlap(AActor* Actor, AActor* OtherActor)
{
	ACA_Bat* bat = Cast<ACA_Bat>(OtherActor);
	if (IsValid(bat))
		ApplyEffect(bat);
}

void ACA_Bonus::EndOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
	ACA_MapBase *map = Cast<ACA_MapBase>(OtherActor);

	if (IsValid(map) && OtherComp == map->DeathBox)
	{
		Destroy();
	}
}

void ACA_Bonus::Tick_Implementation(float DeltaTime)
{
	Super::Tick(DeltaTime);

	BonusFall(DeltaTime);
}

void ACA_Bonus::ApplyEffect_Implementation(ACA_Bat* Player)
{
	Player->AddBonus(this);
	Destroy();
}

EBonusClass ACA_Bonus::GetBonusClass_Implementation() const
{
	return EBonusClass::Default;
}

void ACA_Bonus::EndGame(EGameEnding Ending)
{
	Destroy();
}

void ACA_Bonus::BeginPlay_Implementation()
{
	Super::BeginPlay();

	UCA_GameInstance* gi = Cast<UCA_GameInstance>(GetGameInstance());
	gi->m_OnGameEnd.AddDynamic(this, &ACA_Bonus::EndGame);

}

void ACA_Bonus::BonusFall(float DeltaTime)
{
	AddActorWorldTransform(FTransform(SpinSpeed * DeltaTime, FVector(0, 0, -FallSpeed * DeltaTime)));
}

