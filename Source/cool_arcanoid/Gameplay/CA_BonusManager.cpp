// Fill out your copyright notice in the Description page of Project Settings.


#include "CA_BonusManager.h"

#include "CA_Bonus.h"
#include "CA_Bat.h"
#include "CA_Ball.h"
#include "../Game/CA_GameInstance.h"
#include "../Map/CA_MapBase.h"

// Sets default values for this component's properties
UCA_BonusManager::UCA_BonusManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	PrimaryComponentTick.bStartWithTickEnabled = false;
}


// Called when the game starts
void UCA_BonusManager::BeginPlay()
{
	Super::BeginPlay();
}

void AdjustLocation(ACA_Bat* bat, FVector scale)
{
	UCA_GameInstance* gi = Cast<UCA_GameInstance>(bat->GetGameInstance());
	ACA_MapBase* map = gi->CurMap;
	FVector cur_loc = bat->GetActorLocation();

	if (cur_loc.X + bat->BaseLen * scale.X / 2 > map->MapSizeX / 2)
		bat->SetActorLocation(FVector(map->MapSizeX / 2 - bat->BaseLen * scale.X / 2,
			cur_loc.Y, cur_loc.Z));
	else if (cur_loc.X - bat->BaseLen * scale.X / 2 < -map->MapSizeX / 2)
		bat->SetActorLocation(FVector(-map->MapSizeX / 2 + bat->BaseLen * scale.X / 2,
			cur_loc.Y, cur_loc.Z));
}

void UCA_BonusManager::ExpendTick()
{
	if (!ExpandState.IsExpand)
	{
		ExpandState.IsExpand = true;
		ExpandState.Alpha = 0;
	}
	else if (ExpandState.TotalTime < ExpandState.SmoothDeltaTime)
		ExpandState.TotalTime = ExpandState.Alpha * ExpandState.SmoothDuration;

	if (ExpandState.TotalTime < ExpandState.SmoothDuration)
	{
		ExpandState.Alpha += ExpandState.SmoothDeltaTime / ExpandState.SmoothDuration;
	}
	else if (ExpandState.TotalTime <= ExpandState.SmoothDuration + ExpandState.Duration)
	{
		ExpandState.Alpha = 1.f;
	}
	else if (ExpandState.TotalTime <= 2 * ExpandState.SmoothDuration + ExpandState.Duration)
	{
		ExpandState.Alpha -= ExpandState.SmoothDeltaTime / ExpandState.SmoothDuration;
	}
	else {
		ExpandState.Alpha = 0;
	}

	ExpandState.TotalTime += ExpandState.SmoothDeltaTime;

	ACA_Bat* bat = Cast<ACA_Bat>(GetOwner());

	FVector scale = FMath::Lerp(bat->BaseScale,
		FVector(bat->BaseScale.X * ExpandState.ExpandScale, bat->BaseScale.Y, bat->BaseScale.Z),
		ExpandState.Alpha);

	bat->SetActorScale3D(scale);
	
	if (ExpandState.TotalTime > 2 * ExpandState.SmoothDuration + ExpandState.Duration)
	{
		bat->SetActorScale3D(bat->BaseScale);
		GetWorld()->GetTimerManager().ClearTimer(ExpandState.TimerHandle);
	}

	AdjustLocation(bat, scale);
}

void UCA_BonusManager::AddBonus_Expand(ACA_Bonus* Bonus)
{
	ExpandState.TotalTime = 0;
	GetWorld()->GetTimerManager().SetTimer(
		ExpandState.TimerHandle, 
		this, 
		&UCA_BonusManager::ExpendTick,
		ExpandState.SmoothDeltaTime, 
		true);
}

void UCA_BonusManager::NarrowTick()
{
	if (!ExpandState.IsExpand)
	{
		ExpandState.IsExpand = true;
		ExpandState.Alpha = 0;
	}
	else if (ExpandState.TotalTime < ExpandState.SmoothDeltaTime)
		ExpandState.TotalTime = -ExpandState.Alpha * ExpandState.SmoothDuration;

	if (ExpandState.TotalTime < ExpandState.SmoothDuration)
	{
		ExpandState.Alpha -= ExpandState.SmoothDeltaTime / ExpandState.SmoothDuration;
	}
	else if (ExpandState.TotalTime <= ExpandState.SmoothDuration + ExpandState.Duration)
	{
		ExpandState.Alpha = -1.f;
	}
	else if (ExpandState.TotalTime <= 2 * ExpandState.SmoothDuration + ExpandState.Duration)
	{
		ExpandState.Alpha += ExpandState.SmoothDeltaTime / ExpandState.SmoothDuration;
	}
	else {
		ExpandState.Alpha = 0;
	}

	ExpandState.TotalTime += ExpandState.SmoothDeltaTime;

	ACA_Bat* bat = Cast<ACA_Bat>(GetOwner());

	FVector scale = FMath::Lerp(bat->BaseScale,
		FVector(bat->BaseScale.X * ExpandState.NarrowScale, bat->BaseScale.Y, bat->BaseScale.Z),
		-ExpandState.Alpha);

	bat->SetActorScale3D(scale);

	if (ExpandState.TotalTime > 2 * ExpandState.SmoothDuration + ExpandState.Duration)
	{
		bat->SetActorScale3D(bat->BaseScale);
		GetWorld()->GetTimerManager().ClearTimer(ExpandState.TimerHandle);
	}

	AdjustLocation(bat, scale);
}

void UCA_BonusManager::AddBonus_Narrow(ACA_Bonus* Bonus)
{
	ExpandState.TotalTime = 0;
	GetWorld()->GetTimerManager().SetTimer(
		ExpandState.TimerHandle,
		this,
		&UCA_BonusManager::NarrowTick,
		ExpandState.SmoothDeltaTime,
		true);
}

void UCA_BonusManager::AddBonus_BallFast(ACA_Bonus* Bonus)
{
	ACA_Bat* bat = Cast<ACA_Bat>(GetOwner());

	for (auto& b : bat->AllBalls)
		b->Speed *= BallSpeedState.FastSpeedScale;

	GetWorld()->GetTimerManager().SetTimer(
		BallSpeedState.TimerHandle,
		this,
		&UCA_BonusManager::Remove_SpeedBonus,
		BallSpeedState.Duration);
}

void UCA_BonusManager::AddBonus_BallSlow(ACA_Bonus* Bonus)
{
	ACA_Bat* bat = Cast<ACA_Bat>(GetOwner());

	for (auto& b : bat->AllBalls)
		b->Speed *= BallSpeedState.SlowSpeedScale;

	GetWorld()->GetTimerManager().SetTimer(
		BallSpeedState.TimerHandle,
		this,
		&UCA_BonusManager::Remove_SpeedBonus,
		BallSpeedState.Duration);
}

void UCA_BonusManager::Remove_SpeedBonus()
{
	ACA_Bat* bat = Cast<ACA_Bat>(GetOwner());

	for (auto& b : bat->AllBalls)
		b->Speed = b->BaseSpeed;
}

void UCA_BonusManager::Remove_Expand()
{
	ACA_Bat* bat = Cast<ACA_Bat>(GetOwner());

	GetWorld()->GetTimerManager().ClearTimer(ExpandState.TimerHandle);
	bat->SetActorScale3D(bat->BaseScale);  
	ExpandState = FCA_ExpandBonusState();
}

void UCA_BonusManager::AddBonus_BallPenetration(ACA_Bonus* Bonus)
{
	UCA_GameInstance* gi = Cast<UCA_GameInstance>(GetOwner()->GetGameInstance());

	for (auto& b : gi->CurMap->AllBlocks)
	{
		b->BlockMesh->SetCollisionProfileName(TEXT("OverlapAll"));
		b->SetTransparent(true);
	}
	GetWorld()->GetTimerManager().SetTimer(
		BallPenetrationState.TimerHandle,
		this,
		&UCA_BonusManager::Remove_BallPenetration,
		BallPenetrationState.Duration);

}

void UCA_BonusManager::Remove_BallPenetration()
{
	UCA_GameInstance* gi = Cast<UCA_GameInstance>(GetOwner()->GetGameInstance());

	for (auto& b : gi->CurMap->AllBlocks)
	{
		b->BlockMesh->SetCollisionProfileName(TEXT("BlockAllDynamic"));
		b->SetTransparent(false);
	}
}

void UCA_BonusManager::AddBonus_Explosion(ACA_Bonus* Bonus)
{
	ACA_Bat* bat = Cast<ACA_Bat>(GetOwner());
	
	for (auto& ball : bat->AllBalls)
	{
		ball->MakeExplosive();
	}
}

// Called every frame
void UCA_BonusManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UCA_BonusManager::AddBonus(ACA_Bonus* Bonus)
{
	ACA_Bat* bat = Cast<ACA_Bat>(GetOwner());

	switch (Bonus->GetBonusClass())
	{
	case EBonusClass::Expand:
		AddBonus_Expand(Bonus);
		break;
	case EBonusClass::Narrow:
		AddBonus_Narrow(Bonus);
		break;
	case EBonusClass::ExtraBall:
		bat->SpawnBall();
		break;
	case EBonusClass::Explosion:
		AddBonus_Explosion(Bonus);
		break;
	case EBonusClass::BallFast:
		AddBonus_BallFast(Bonus);
		break;
	case EBonusClass::BallSlow:
		AddBonus_BallSlow(Bonus);
		break;
	case EBonusClass::BallPenetration:
		AddBonus_BallPenetration(Bonus);
		break;
	case EBonusClass::Default:
	default:
		break;
	}
}

void UCA_BonusManager::RemoveAllBonuses()
{
	Remove_Expand();
}

