// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CA_Block.generated.h"

UCLASS()
class COOL_ARCANOID_API ACA_Block : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ACA_Block();

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	UStaticMeshComponent* BlockMesh;

	UPROPERTY(BlueprintReadOnly)
	class UDataTable* BonusSpawnTable;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float BaseSizeX = 100;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float BaseSizeY = 100;
	UPROPERTY(BlueprintReadWrite)
	float SizeX = 100;
	UPROPERTY(BlueprintReadWrite)
	float SizeY = 100;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	int HP = 1;

protected:
	UPROPERTY(BlueprintReadWrite)
	bool IsTrasparent = false;
	
	UPROPERTY(BlueprintReadWrite)
	bool IsMoving = false;
	UPROPERTY(BlueprintReadWrite)
	FVector StartPoint;
	UPROPERTY(BlueprintReadWrite)
	FVector EndPoint;
	UPROPERTY(BlueprintReadWrite)
	float CurAlphaMoving = 0;
	UPROPERTY(BlueprintReadWrite)
	float MovingSpeed = 500;
	UPROPERTY(BlueprintReadWrite)
	float LastHitTime = -1;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void BeginPlay() override;
	void BeginPlay_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void OnHit(AActor* Actor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);
	void OnHit_Implementation(AActor* Actor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	void Break();
	
	UFUNCTION()
	void SpawnBonus();

	UFUNCTION(BlueprintCallable)
	void TakeDamage_HP();
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void SetSize(float newSizeX, float newSizeY);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SetHP(int newHP);
	void SetHP_Implementation(int newHP);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SetMoving(bool newIsMoving, const FVector &P1, const FVector& P2);
	void SetMoving_Implementation(bool newIsMoving, const FVector& P1, const FVector& P2);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SetTransparent(bool newIsTr);
	void SetTransparent_Implementation(bool newIsTr);

};
