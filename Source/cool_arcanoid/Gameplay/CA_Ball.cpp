// Fill out your copyright notice in the Description page of Project Settings.


#include "CA_Ball.h"

#include "Math/UnrealMathUtility.h"

// Sets default values
ACA_Ball::ACA_Ball()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BallMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BatMesh"));
	SetRootComponent(BallMesh);
	BallMesh->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	SetActorEnableCollision(true);
}

// Called when the game starts or when spawned
void ACA_Ball::BeginPlay()
{
	Super::BeginPlay();
	
}

void ACA_Ball::MakeExplosive_Implementation()
{
	IsExplosive = true;
}

// Called every frame
void ACA_Ball::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FHitResult hr;
	AddActorWorldOffset(Dir * Speed * DeltaTime, true, &hr);

	if (hr.bBlockingHit)
	{
		Dir = FMath::GetReflectionVector(Dir, hr.Normal);
		Dir.Y = 0;
		Dir.Normalize();
	}
}

