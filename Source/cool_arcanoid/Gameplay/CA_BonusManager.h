// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CA_BonusManager.generated.h"

class ACA_Bonus;

USTRUCT(BlueprintType)
struct FCA_ExpandBonusState {
	
	GENERATED_USTRUCT_BODY()

public:
	bool IsExpand;
	float Alpha;
	float TotalTime;
	float Duration;
	float SmoothDuration;
	float SmoothDeltaTime;
	float ExpandScale;
	float NarrowScale;
	FTimerHandle TimerHandle;

	FCA_ExpandBonusState() :
		IsExpand(false),
		Alpha(0),
		TotalTime(0),
		Duration(2.f),
		SmoothDuration(1.f),
		SmoothDeltaTime(0.01f),
		ExpandScale(1.5f),
		NarrowScale(0.5f)
	{}
};

USTRUCT(BlueprintType)
struct FCA_BallSpeedState {
	GENERATED_USTRUCT_BODY()

public:
	float FastSpeedScale;
	float SlowSpeedScale;
	float Duration;
	FTimerHandle TimerHandle;

	FCA_BallSpeedState(): 
		FastSpeedScale(2.f),
		SlowSpeedScale(0.5f),
		Duration(3.f)
	{}
};

USTRUCT(BlueprintType)
struct FCA_BallPenetrationState {
	GENERATED_USTRUCT_BODY()

public:
	float Duration;
	FTimerHandle TimerHandle;

	FCA_BallPenetrationState() :
		Duration(3.f)
	{}
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class COOL_ARCANOID_API UCA_BonusManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCA_BonusManager();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	void ExpendTick();
	void AddBonus_Expand(ACA_Bonus* Bonus);
	void NarrowTick();
	void AddBonus_Narrow(ACA_Bonus* Bonus);
	void AddBonus_BallFast(ACA_Bonus* Bonus);
	void AddBonus_BallSlow(ACA_Bonus* Bonus);
	void Remove_SpeedBonus();
	void Remove_Expand();
	void AddBonus_BallPenetration(ACA_Bonus* Bonus);
	void Remove_BallPenetration();
	
	UPROPERTY(EditAnywhere)
	FCA_ExpandBonusState ExpandState;
	FCA_BallSpeedState BallSpeedState;
	FCA_BallPenetrationState BallPenetrationState;

	void AddBonus_Explosion(ACA_Bonus* Bonus);

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void AddBonus(ACA_Bonus* Bonus);

	UFUNCTION(BlueprintCallable)
	void RemoveAllBonuses();

};
