// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CA_Ball.generated.h"

UCLASS()
class COOL_ARCANOID_API ACA_Ball : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACA_Ball();

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	UStaticMeshComponent* BallMesh;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	FVector Dir;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float Speed;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float BaseSpeed = 1000.;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite)
	bool IsExplosive = false;

public:	
	UFUNCTION(BlueprintNativeEvent)
	void MakeExplosive();
	void MakeExplosive_Implementation();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
