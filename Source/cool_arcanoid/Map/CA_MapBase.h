// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/Classes/Components/BoxComponent.h"

#include "CA_MapBase.generated.h"

class UCA_Wall;
class ACA_Block;

UCLASS()
class COOL_ARCANOID_API ACA_MapBase : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ACA_MapBase();

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float MapSizeX = 3000;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float MapSizeZ = 3000;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float CameraBorderLen = 100;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float BlockGap = 10;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float InnerHorizontalBorder = 50;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float InnerBackBorder = 50;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float InnerFrontBorder = 300;
	UPROPERTY(BlueprintReadWrite)
	int BlockRowNum = 10;
	UPROPERTY(BlueprintReadWrite)
	int BlockColNum = 10;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	UCA_Wall* LeftWall;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	UCA_Wall* RightWall;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	UCA_Wall* BackWall;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	UBoxComponent* DeathBox;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* CameraComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void SetupCamera();

	void SpawnBlocks_Moving();
	void SpawnBlocks_Random();
	void SpawnBlocks_1();
	void SpawnBlocks_2();
	void SpawnBlocks_3();


	UFUNCTION()
	void SpawnBlocks();

	UFUNCTION()
	void StartGame();
	UFUNCTION()
	void EndGame(EGameEnding Ending);

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	USceneComponent* DefaultRootComponent;

	class ACA_Bat* Player;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TSubclassOf<ACA_Bat> PlayerClass;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TSubclassOf<ACA_Block> BlockClass;

	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:	
	UPROPERTY()
	TArray<ACA_Block*> AllBlocks;

	UFUNCTION()
	void OnBlockBreak(ACA_Block* DestroedBlock);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void OnConstruction(const FTransform& FTrans) override;

};
