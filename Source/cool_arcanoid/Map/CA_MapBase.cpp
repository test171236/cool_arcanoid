// Fill out your copyright notice in the Description page of Project Settings.


#include "CA_MapBase.h"

#include "CA_Wall.h"
#include "../Gameplay/CA_Bat.h"
#include "../Gameplay/CA_Ball.h"
#include "../Gameplay/CA_Block.h"
#include "../Game/CA_GameStateBase.h"
#include "../Game/CA_GameInstance.h"
#include "Kismet/KismetMathLibrary.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ACA_MapBase::ACA_MapBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	 
	DefaultRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRootComponent"));
	SetRootComponent(DefaultRootComponent);

	BackWall = CreateDefaultSubobject<UCA_Wall>(TEXT("BackWall"));
	BackWall->SetupAttachment(DefaultRootComponent);
	BackWall->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	LeftWall = CreateDefaultSubobject<UCA_Wall>(TEXT("LeftWall"));
	LeftWall->SetupAttachment(DefaultRootComponent);
	LeftWall->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	RightWall = CreateDefaultSubobject<UCA_Wall>(TEXT("RightWall"));
	RightWall->SetupAttachment(DefaultRootComponent);
	RightWall->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

	DeathBox = CreateDefaultSubobject<UBoxComponent>(TEXT("DeathBox"));
	DeathBox->SetupAttachment(DefaultRootComponent);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(DefaultRootComponent);

	CameraComponent->SetProjectionMode(ECameraProjectionMode::Orthographic);
	SetActorEnableCollision(true);

	DeathBox->OnComponentBeginOverlap.AddDynamic(this, &ACA_MapBase::OnOverlapBegin);

}

void ACA_MapBase::SetupCamera(void)
{
	if (IsValid(CameraComponent))
	{
		FVector loc = FVector(0, 2000, MapSizeZ / 2);
		FTransform FTrans = FTransform(
			UKismetMathLibrary::FindLookAtRotation(loc, FVector(0, 0, MapSizeZ / 2)),
			loc);

		CameraComponent->SetWorldTransform(FTrans);

		float h = (MapSizeX + 2 * CameraBorderLen) / CameraComponent->AspectRatio;
		float w = (MapSizeX + 2 * CameraBorderLen);
		if (h < (MapSizeZ + 2 * CameraBorderLen))
			CameraComponent->SetOrthoWidth((MapSizeZ + 2 * CameraBorderLen) * CameraComponent->AspectRatio);
		else
			CameraComponent->SetOrthoWidth(w);
	}

	APlayerController* pc = UGameplayStatics::GetPlayerController(this, 0);
	if(IsValid(pc))
		pc->SetViewTarget(this);
}

void ACA_MapBase::SpawnBlocks_Random(void)
{
	float BlockW = (MapSizeX - 2 * InnerHorizontalBorder) / BlockColNum;
	float BlockH = (MapSizeZ - InnerBackBorder - InnerFrontBorder) / BlockRowNum;
	FVector start_loc = FVector(-MapSizeX / 2 + InnerHorizontalBorder + BlockW / 2,
		0, InnerFrontBorder + BlockH / 2);

	for (int x = 0; x < BlockColNum; x++)
		for (int y = 0; y < BlockRowNum; y++)
		{
			ACA_Block* block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
				FTransform(start_loc + FVector(x * BlockW, 0, y * BlockH)), this);
			block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
			block->SetHP_Implementation((FMath::Rand() % 5) + 1);
			block->FinishSpawning(FTransform(start_loc + FVector(x * BlockW, 0, y * BlockH)));

			AllBlocks.Add(block);
		}
}

void ACA_MapBase::SpawnBlocks_Moving(void)
{
	float BlockW = (MapSizeX - 2 * InnerHorizontalBorder) / BlockColNum;
	float BlockH = (MapSizeZ - InnerBackBorder - InnerFrontBorder) / BlockRowNum;
	FVector start_loc = FVector(-MapSizeX / 2 + InnerHorizontalBorder + BlockW / 2,
		0, InnerFrontBorder + BlockH / 2);

	for (int y = 0; y < BlockRowNum; y++)
	{
		ACA_Block* block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector(0, 0, y * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation((FMath::Rand() % 5) + 1);
		block->SetMoving_Implementation(true, start_loc + FVector(0, 0, y * BlockH),
			start_loc + FVector((BlockColNum - 1) * BlockW, 0, y * BlockH));
		block->FinishSpawning(FTransform(start_loc + FVector(0, 0, y * BlockH)));

		AllBlocks.Add(block);
	}
}

void ACA_MapBase::SpawnBlocks_1(void)
{
	BlockRowNum = 11;
	BlockColNum = 10;

	float BlockW = (MapSizeX - 2 * InnerHorizontalBorder) / BlockColNum;
	float BlockH = (MapSizeZ - InnerBackBorder - InnerFrontBorder) / BlockRowNum;
	FVector start_loc = FVector(-MapSizeX / 2 + InnerHorizontalBorder + BlockW / 2,
		0, InnerFrontBorder + BlockH / 2);

	for (int x = 0; x < BlockColNum; x++)
		for (int y = 1; y < BlockRowNum - 1; y++)
			if ((x+y) % 2 && y != BlockRowNum / 2)
			{
				ACA_Block* block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
					FTransform(start_loc + FVector(x * BlockW, 0, y * BlockH)), this);
				block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
				if (y < BlockRowNum / 2)
					block->SetHP_Implementation(((y - 1) % 5) + 1);
				else
					block->SetHP_Implementation(((BlockRowNum - y - 2) % 5) + 1);
				block->FinishSpawning(FTransform(start_loc + FVector(x * BlockW, 0, y * BlockH)));

				AllBlocks.Add(block);
			}

	int const_firstlast = 4;
	float BlockW1 = (MapSizeX - 2 * InnerHorizontalBorder) / const_firstlast;
	FVector start_loc1 = FVector(-MapSizeX / 2 + InnerHorizontalBorder + BlockW1 / 2,
		0, InnerFrontBorder + BlockH / 2);

	for (int x = 0; x < const_firstlast; x++)
	{
		ACA_Block* block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc1 + FVector(x * BlockW1, 0, 0)), this);
		block->SetSize(BlockW1 - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(3);
		block->FinishSpawning(FTransform(start_loc1 + FVector(x * BlockW1, 0, 0)));

		AllBlocks.Add(block);

		block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc1 + FVector(x * BlockW1, 0, (BlockRowNum - 1)*BlockH)), this);
		block->SetSize(BlockW1 - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(2);
		block->FinishSpawning(FTransform(start_loc1 + FVector(x * BlockW1, 0, (BlockRowNum - 1)*BlockH)));

		AllBlocks.Add(block);
	}


	int y = BlockRowNum / 2;
	{
		ACA_Block* block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector(0 * BlockW, 0, y * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(5);
		block->SetMoving_Implementation(true, start_loc + FVector(0 * BlockW, 0, y * BlockH),
			start_loc + FVector((BlockColNum - 1) / 2 * BlockW, 0, y * BlockH));
		block->FinishSpawning(FTransform(start_loc + FVector(0 * BlockW, 0, y * BlockH)));

		AllBlocks.Add(block);

		block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector((BlockColNum - 1) * BlockW, 0, y * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(5);
		block->SetMoving_Implementation(true, start_loc + FVector((BlockColNum - 1) * BlockW, 0, y * BlockH),
			start_loc + FVector((BlockColNum + 1) / 2 * BlockW, 0, y * BlockH));
		block->FinishSpawning(FTransform(start_loc + FVector((BlockColNum - 1) * BlockW, 0, y * BlockH)));

		AllBlocks.Add(block);
	}
}

void ACA_MapBase::SpawnBlocks_2(void)
{
	BlockColNum = 11;
	BlockRowNum = BlockColNum+1;

	float BlockW = (MapSizeX - 2 * InnerHorizontalBorder) / BlockColNum;
	float BlockH = (MapSizeZ - InnerBackBorder - InnerFrontBorder) / BlockRowNum;
	FVector start_loc = FVector(-MapSizeX / 2 + InnerHorizontalBorder + BlockW / 2,
		0, InnerFrontBorder + BlockH / 2);
	BlockRowNum -= 1;

	for (int x = 0; x < BlockColNum; x++)
	{
		int y = x;
		ACA_Block* block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector(x * BlockW, 0, y * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(x <= BlockColNum / 2 ? (x % 5) + 1 : ((BlockColNum - x - 1) % 5) + 1);
		block->FinishSpawning(FTransform(start_loc + FVector(x * BlockW, 0, y * BlockH)));

		AllBlocks.Add(block);

		if (y == BlockColNum - x - 1)
			continue;

		y = BlockColNum - x - 1;
		block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector(x * BlockW, 0, y * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(x <= BlockColNum / 2 ? (x % 5) + 1 : ((BlockColNum - x - 1) % 5) + 1);
		block->FinishSpawning(FTransform(start_loc + FVector(x * BlockW, 0, y * BlockH)));

		AllBlocks.Add(block);
	}

	for (int x = 0; x < BlockColNum; x++)
	{
		if (x == (BlockColNum - 1) / 2)
			continue;

		int y = (BlockRowNum-1) / 2;

		ACA_Block* block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector(x * BlockW, 0, y * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(x <= BlockColNum / 2 ? (x % 5) + 1 : ((BlockColNum - x - 1) % 5) + 1);
		block->FinishSpawning(FTransform(start_loc + FVector(x * BlockW, 0, y * BlockH)));

		AllBlocks.Add(block);
	}

	for (int y = 0; y < BlockRowNum + 1; y++)
	{
		if (y == (BlockRowNum - 1) / 2)
			continue;

		int x = (BlockColNum - 1) / 2;

		ACA_Block* block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector(x * BlockW, 0, y * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		if (y == BlockRowNum)
			block->SetHP_Implementation(2);
		else
			block->SetHP_Implementation(y <= BlockColNum / 2 ? (y % 5) + 1 : ((BlockColNum - y - 1) % 5) + 1);
		block->FinishSpawning(FTransform(start_loc + FVector(x * BlockW, 0, y * BlockH)));

		AllBlocks.Add(block);
	}
	// Left Moving
	{
		ACA_Block* block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector(0 * BlockW, 0, 1 * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(3);
		block->SetMoving_Implementation(true, start_loc + FVector(0 * BlockW, 0, 1 * BlockH),
			start_loc + FVector(0 * BlockW, 0, (BlockRowNum - 3) / 2 * BlockH));
		block->FinishSpawning(FTransform(start_loc + FVector(0 * BlockW, 0, 1 * BlockH)));

		AllBlocks.Add(block);

		block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector(0 * BlockW, 0, (BlockRowNum - 2) * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(3);
		block->SetMoving_Implementation(true, start_loc + FVector(0 * BlockW, 0, (BlockRowNum - 2) * BlockH),
			start_loc + FVector(0 * BlockW, 0, (BlockRowNum + 1) / 2 * BlockH));
		block->FinishSpawning(FTransform(start_loc + FVector(0 * BlockW, 0, (BlockRowNum - 2) * BlockH)));

		AllBlocks.Add(block);
	}

	// Right Moving
	{
		ACA_Block* block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector((BlockColNum - 1) * BlockW, 0, 1 * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(3);
		block->SetMoving_Implementation(true, start_loc + FVector((BlockColNum - 1) * BlockW, 0, 1 * BlockH),
			start_loc + FVector((BlockColNum - 1) * BlockW, 0, (BlockRowNum - 3) / 2 * BlockH));
		block->FinishSpawning(FTransform(start_loc + FVector((BlockColNum - 1) * BlockW, 0, 1 * BlockH)));

		AllBlocks.Add(block);

		block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector((BlockColNum - 1) * BlockW, 0, (BlockRowNum - 2) * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(3);
		block->SetMoving_Implementation(true, start_loc + FVector((BlockColNum - 1) * BlockW, 0, (BlockRowNum - 2) * BlockH),
			start_loc + FVector((BlockColNum - 1) * BlockW, 0, (BlockRowNum + 1) / 2 * BlockH));
		block->FinishSpawning(FTransform(start_loc + FVector((BlockColNum - 1) * BlockW, 0, (BlockRowNum - 2) * BlockH)));

		AllBlocks.Add(block);
	}

	// Top Moving
	{
		ACA_Block* block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector((BlockColNum - 2) * BlockW, 0, (BlockRowNum - 1) * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(3);
		block->SetMoving_Implementation(true, start_loc + FVector((BlockColNum - 2) * BlockW, 0, (BlockRowNum - 1) * BlockH),
			start_loc + FVector((BlockColNum + 1) / 2 * BlockW, 0, (BlockRowNum - 1) * BlockH));
		block->FinishSpawning(FTransform(start_loc + FVector((BlockColNum - 2) * BlockW, 0, (BlockRowNum - 1) * BlockH)));

		AllBlocks.Add(block);

		block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector(1 * BlockW, 0, (BlockRowNum - 1) * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(3);
		block->SetMoving_Implementation(true, start_loc + FVector(1 * BlockW, 0, (BlockRowNum - 1) * BlockH),
			start_loc + FVector((BlockColNum - 3) / 2 * BlockW, 0, (BlockRowNum - 1) * BlockH));
		block->FinishSpawning(FTransform(start_loc + FVector(1 * BlockW, 0, (BlockRowNum - 1) * BlockH)));

		AllBlocks.Add(block);
	}

	// Bottom Moving
	{
		ACA_Block* block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector((BlockColNum - 2) * BlockW, 0, 0 * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(3);
		block->SetMoving_Implementation(true, start_loc + FVector((BlockColNum - 2) * BlockW, 0, 0 * BlockH),
			start_loc + FVector((BlockColNum + 1) / 2 * BlockW, 0, 0 * BlockH));
		block->FinishSpawning(FTransform(start_loc + FVector((BlockColNum - 2) * BlockW, 0, 0 * BlockH)));

		AllBlocks.Add(block);

		block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector(1 * BlockW, 0, 0 * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(3);
		block->SetMoving_Implementation(true, start_loc + FVector(1 * BlockW, 0, 0 * BlockH),
			start_loc + FVector((BlockColNum - 3) / 2 * BlockW, 0, 0 * BlockH));
		block->FinishSpawning(FTransform(start_loc + FVector(1 * BlockW, 0, 0 * BlockH)));

		AllBlocks.Add(block);
	}

}

void ACA_MapBase::SpawnBlocks_3(void)
{
	BlockColNum = 10;
	BlockRowNum = 10;

	float BlockW = (MapSizeX - 2 * InnerHorizontalBorder) / BlockColNum;
	float BlockH = (MapSizeZ - InnerBackBorder - InnerFrontBorder) / BlockRowNum;
	FVector start_loc = FVector(-MapSizeX / 2 + InnerHorizontalBorder + BlockW / 2,
		0, InnerFrontBorder + BlockH / 2);

	for (int x = 0; x < BlockColNum; x++)
	{
		int y = x;
		ACA_Block* block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector(x * BlockW, 0, y * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(x <= BlockColNum / 2 ? (x % 5) + 1 : ((BlockColNum - x - 1) % 5) + 1);
		block->FinishSpawning(FTransform(start_loc + FVector(x * BlockW, 0, y * BlockH)));

		AllBlocks.Add(block);

		if (y == BlockColNum - x - 1)
			continue;

		y = BlockColNum - x - 1;
		block = GetWorld()->SpawnActorDeferred<ACA_Block>(BlockClass,
			FTransform(start_loc + FVector(x * BlockW, 0, y * BlockH)), this);
		block->SetSize(BlockW - BlockGap, BlockH - BlockGap);
		block->SetHP_Implementation(x <= BlockColNum / 2 ? (x % 5) + 1 : ((BlockColNum - x - 1) % 5) + 1);
		block->FinishSpawning(FTransform(start_loc + FVector(x * BlockW, 0, y * BlockH)));

		AllBlocks.Add(block);
	}
}

void ACA_MapBase::SpawnBlocks()
{
	if (FMath::Rand() % 2)
		SpawnBlocks_1();
	else
		SpawnBlocks_2();
}

void ACA_MapBase::StartGame(void)
{
	SpawnBlocks();
}

void ACA_MapBase::EndGame(EGameEnding Ending)
{
	for (auto block = AllBlocks.begin(); block != AllBlocks.end(); ++block)
		(*block)->Destroy();
	AllBlocks.Empty();
}

// Called when the game starts or when spawned
void ACA_MapBase::BeginPlay()
{
	Super::BeginPlay();

	UCA_GameInstance *gi =  Cast<UCA_GameInstance>(GetGameInstance());
	gi->m_OnGameStarted.AddDynamic(this, &ACA_MapBase::StartGame);
	gi->m_OnGameEnd.AddDynamic(this, &ACA_MapBase::EndGame);

	Player = GetWorld()->SpawnActor<ACA_Bat>(PlayerClass);

	SetupCamera();

	gi->CurMap = this;
}

void ACA_MapBase::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ACA_Ball* b = Cast<ACA_Ball>(OtherActor);

	if (IsValid(b))
	{
		Player->DestroyBall(b);
	}
}

void ACA_MapBase::OnBlockBreak(ACA_Block* DestroedBlock)
{
	AllBlocks.Remove(DestroedBlock);

	ACA_GameStateBase* gs = Cast<ACA_GameStateBase>(GetWorld()->GetGameState());
	if (IsValid(gs))
		gs->AddScore();

	if (AllBlocks.Num() < 1)
	{
		UCA_GameInstance* gi = Cast<UCA_GameInstance>(GetGameInstance());
		gi->m_OnGameEnd.Broadcast(EGameEnding::Win);
	}
}

// Called every frame
void ACA_MapBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACA_MapBase::OnConstruction(const FTransform& FTrans)
{
	Super::OnConstruction(FTrans);

	FTransform tr = FTransform(FRotator(), FVector(0, 0, MapSizeZ + BackWall->BaseLen / 2), FVector(MapSizeX / BackWall->BaseLen + 2, 1, 1));
	BackWall->SetRelativeTransform(tr);

	tr = FTransform(FRotator(), FVector(-MapSizeX / 2 - LeftWall->BaseLen/2, 0, MapSizeZ/2), FVector(1, 1, MapSizeZ / LeftWall->BaseLen));
	LeftWall->SetRelativeTransform(tr);

	tr = FTransform(FRotator(), FVector(MapSizeX / 2 + RightWall->BaseLen/2, 0, MapSizeZ/2), FVector(1, 1, MapSizeZ / RightWall->BaseLen));
	RightWall->SetRelativeTransform(tr);

	tr = FTransform(FRotator(), FVector(0, 0, -30), FVector(MapSizeX / 60 + 2, 1, 1));
	DeathBox->SetRelativeTransform(tr);
}

