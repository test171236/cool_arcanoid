// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "Game/CA_GameInstance.h"

#include "cool_arcanoidGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class COOL_ARCANOID_API Acool_arcanoidGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintNativeEvent)
	void BeginPlay() override;

	void BeginPlay_Implementation();

	UFUNCTION(BlueprintImplementableEvent)
	void StartGame();

	UFUNCTION(BlueprintImplementableEvent)
	void EndGame(EGameEnding Ending);
};
