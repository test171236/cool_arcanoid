// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../Gameplay/CA_Block.h"
#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "CA_GameInstance.generated.h"

UENUM(BlueprintType)
enum class EGameEnding : uint8 {
	Lose UMETA(DisplayName = "Lose"),
	Win	 UMETA(DisplayName = "Win"),
};

class ACA_MapBase;

/**
 * 
 */
UCLASS()
class COOL_ARCANOID_API UCA_GameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameStarted);
	UPROPERTY()
	FOnGameStarted m_OnGameStarted;

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGameEnd, EGameEnding, Ending);
	UPROPERTY()
	FOnGameEnd m_OnGameEnd;

	UPROPERTY()
	ACA_MapBase* CurMap;

};
