// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../Map/CA_MapBase.h"
#include "../UI/CA_UserWidget.h"
#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "CA_GameStateBase.generated.h"

/**
 * 
 */
UCLASS()
class COOL_ARCANOID_API ACA_GameStateBase : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	ACA_GameStateBase();

	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite)
	bool IsGameStarted;

	UPROPERTY(BlueprintReadWrite)
	int CurScore;
	UPROPERTY(BlueprintReadWrite)
	int PrevScore;
	UPROPERTY(BlueprintReadWrite)
	int Lifes;

	UPROPERTY(BlueprintReadOnly)
	UCA_UserWidget* HUD;

	TSubclassOf<UCA_UserWidget>	HUDClass;

	UFUNCTION()
	void AddScore();

	UFUNCTION()
	void Died();

private:

	UFUNCTION()
	void StartGame();
	
	UFUNCTION()
	void EndGame(EGameEnding Ending);
};
