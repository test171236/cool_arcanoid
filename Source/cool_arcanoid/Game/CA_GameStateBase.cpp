// Fill out your copyright notice in the Description page of Project Settings.


#include "CA_GameStateBase.h"
#include "../UI/CA_PlayerController.h"
#include "CA_GameInstance.h"
#include "../cool_arcanoidGameModeBase.h"

ACA_GameStateBase::ACA_GameStateBase()
{
    ConstructorHelpers::FClassFinder<UUserWidget> MenuClassFinder(TEXT("WidgetBlueprint'/Game/Blueprints/UI/BP_UserWidget.BP_UserWidget_C'"));
    HUDClass = MenuClassFinder.Class;

    CurScore = 0;
    PrevScore = 0;
    Lifes = 3;
}

void ACA_GameStateBase::BeginPlay()
{
    HUD = CreateWidget<UCA_UserWidget>(GetWorld(), HUDClass, TEXT("HUD"));
    HUD->AddToViewport();

    UCA_GameInstance* gi = Cast<UCA_GameInstance>(GetGameInstance());
    gi->m_OnGameStarted.AddDynamic(this, &ACA_GameStateBase::StartGame);
    gi->m_OnGameEnd.AddDynamic(this, &ACA_GameStateBase::EndGame);
}

void ACA_GameStateBase::AddScore()
{
    CurScore++;
    HUD->UpdateScore(CurScore);
}

void ACA_GameStateBase::Died(void)
{
    Lifes--;
    HUD->UpdateLifes(Lifes);
    if (Lifes <= 0)
    {
        UCA_GameInstance* gi = Cast<UCA_GameInstance>(GetGameInstance());
        gi->m_OnGameEnd.Broadcast(EGameEnding::Lose);
    }
}

void ACA_GameStateBase::StartGame()
{
    Lifes = 3;
    HUD->UpdateLifes(Lifes);
    PrevScore = CurScore;
    CurScore = 0;
    HUD->UpdateScore(CurScore);
    HUD->UpdatePrevScore(PrevScore);

    IsGameStarted = true;
}

void ACA_GameStateBase::EndGame(EGameEnding Ending)
{
    IsGameStarted = false;
}