// Copyright Epic Games, Inc. All Rights Reserved.


#include "cool_arcanoidGameModeBase.h"

void Acool_arcanoidGameModeBase::BeginPlay_Implementation()
{
    UCA_GameInstance* gi = Cast<UCA_GameInstance>(GetGameInstance());

    gi->m_OnGameStarted.AddDynamic(this, &Acool_arcanoidGameModeBase::StartGame);
    gi->m_OnGameEnd.AddDynamic(this, &Acool_arcanoidGameModeBase::EndGame);
}
